# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=4

DESCRIPTION="Moneydance is easy to use personal finance software."
HOMEPAGE="http://infinitekind.com/moneydance"

URL_64="http://infinitekind.com/stabledl/2015/Moneydance_linux_amd64.tar.gz"
URL_32="http://infinitekind.com/stabledl/2015/Moneydance_linux_x86.tar.gz"

SRC_URI="
	amd64? ( ${URL_64} )
	x86? ( ${URL_32} )
"

LICENSE="Moneydance"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="mirror"

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_unpack() {
        unpack ${A}
}

src_install () {

        local TARGETDIR="/opt/Moneydance"
        dodir "${TARGETDIR}"
        insinto "${TARGETDIR}"/

		doins -r ${S}/${PN}/{.*,*}  || die "Install failed!"

		fowners root:users -R "${TARGETDIR}" || die "Could not change ownership of /opt/moneydance directory."
		

        insinto /usr/share/pixmaps
        doins "${FILESDIR}"/moneydance_icon32.png || die "Could not copy icon."

        return
}

pkg_postinst() {
        xdg-desktop-menu install "${FILESDIR}"/abadonna-moneydance.desktop || die "Could not register a menu item"

         chmod 755 /opt/Moneydance/Moneydance || die "Could not set file permissions on Moneydance file"

        return
}

pkg_postrm() {
        xdg-desktop-menu uninstall "${FILESDIR}"/abadonna-moneydance.desktop || die "Could not de-register a menu item"

        return
}

