# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils gnome2-utils systemd unpacker

# Major version
MV=${PV/\.*}
MY_PN=${PN}${MV}

DESCRIPTION="All-In-One Solution for Remote Access and Support over the Internet"
HOMEPAGE="http://www.teamviewer.com"
SRC_URI="http://download.teamviewer.com/download/teamviewer_linux.tar.gz -> ${P}.tar.gz"

LICENSE="TeamViewer ( LGPL-2.1 )"
SLOT="0"
KEYWORDS="~*"
IUSE=""

RESTRICT="mirror"

RDEPEND="app-shells/bash x11-misc/xdg-utils"

S=${WORKDIR}

src_unpack() {
	unpack ${A}
	return
}

src_install () {
	local TARGETDIR="/opt/teamviewer"
	insinto "${TARGETDIR}"/
	exeinto "${TARGETDIR}"/
	dodir "${TARGETDIR}"

	doins -r ${S}/${PN}/*  || die "Install failed!"

	fowners root:users -R "${TARGETDIR}" || die "Could not change ownership of directory."
	fperms g+w -R "${TARGETDIR}" || die "Could not change permission for group."
	fperms 755 "${TARGETDIR}"/teamviewer || die "Could not change permissions for teamviewer file."
	fperms 755 "${TARGETDIR}"/tv-setup || die "Could not change permissions for tv-setup file."
	fperms 755 "${TARGETDIR}"/tv_bin/teamviewerd || die "Could not change permissions for teamvieverd file."
	fperms 755 "${TARGETDIR}"/tv_bin/script/libdepend || die "Could not change permissions for libdepend file."
	fperms 755 "${TARGETDIR}"/tv_bin/script/teamviewer || die "Could not change permissions for teamviewer file."
	fperms 755 "${TARGETDIR}"/tv_bin/script/teamviewerd.sysv || die "Could not change permissions for teamviewerd.sysv file."
	fperms 755 "${TARGETDIR}"/tv_bin/script/teamviewer_setup || die "Could not change permissions for teamviewer_setup file."
	fperms 755 "${TARGETDIR}"/tv_bin/script/wait-console-kit.sh || die "Could not change permissions for wait-console-kit.sh file."
	fperms 755 -R "${TARGETDIR}"/tv_bin/wine/bin || die "Could not change permissions for wine/bin file."

	insinto /usr/share/pixmaps
	doins "${FILESDIR}"/teamviewer.png || die "Could not copy teamviewer.png"

	return
}

pkg_postinst() {
	xdg-desktop-menu install "${FILESDIR}"/abadonna-teamviewer.desktop || die "Could not register a menu item"

	return
}

pkg_postrm() {
	xdg-desktop-menu uninstall "${FILESDIR}"/abadonna-teamviewer.desktop || die "Could not de-register a menu item"
	rm -rf /opt/teamviewer

	return
}

